<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Waypoint Admin Dashboard</title>
        <style>
          #tbl_redirs{
            border-collapse: collapse;
            border-style: hidden;
          }
          #tbl_redirs td {
            padding: 30px;
            border: 3px solid #0044b3;
          }
        </style>

    </head>
    <body>
      <h1>Waypoint Admin Dashboard</h1>

      <p><a href="/admin/new">New Redirect</a></p>

      <h3>Redirects</h3>
      @if(count($redirs) > 0)
      <table id="tbl_redirs">
        <tr><th>Code</th><th>Slug</th><th>Destination</th><th>Clicks</th><th/></tr>

        @foreach($redirs as $redir)
        <tr>
          <td>{{$redir->code}}</td>
          <td>{{$redir->slug}}</td>
          <td>{{$redir->dest}}</td>
          <td>{{$redir->clicks}}</td>
          <td>
            <a href="/admin/edit/{{$redir->id}}">Edit</a>
            <form action="/api/delete/{{$redir->id}}" method="post">
              <button type="submit">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach

      </table>
      @else
      <p>(No redirects defined)</p>
      @endif

    </body>

</html>
