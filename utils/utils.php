<?php

use Illuminate\Http\Request;

function legit(Request $req){
  return $req->session()->get('key') === md5(env('ADMIN_PWD'));
}
