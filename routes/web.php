<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Redir;

require_once __DIR__ . '/../utils/utils.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $req) {
  if (legit($req)){
    return redirect('/admin');
  }else{
    return view('login');
  }
});

Route::get('/admin/success', function(){
  return view('success');
});

Route::get('/admin', function(Request $req){
  if (legit($req)){
    $redirs = Redir::get();
    return view('admin', ['redirs'=>$redirs]);
  }else{
    return response('Bad auth', 403);
  }
});

Route::post('/admin', function (Request $req){
  $passed_pwd = $req->input('pwd');
  $admin_pwd = env('ADMIN_PWD');
  if ($passed_pwd === $admin_pwd){
    $req->session()->put('key', md5($admin_pwd));
    return redirect('/admin'); // GET
  }else{
    return response('Bad auth', 403);
  }
});

Route::get('/admin/new', function (Request $req){
  if (legit($req)){
    return view('modify_redir', ['mode'=>'NEW']);
  }else{
    return response('Bad auth', 403);
  }
});

Route::get('/admin/edit/{id}', function($id, Request $req){
  if (legit($req)){
    $redir = Redir::find($id);
    if ($redir){
      return view('modify_redir', ['mode'=>'EDIT', 'redir'=>$redir]);
    }else{
      return response('Bad id', 404);
    }
  }else{
    return response('Bad auth', 403);
  }
});

// main redir
Route::get('/{slug}', function($slug){
  $redir = Redir::where('slug', $slug)->first();
  if ($redir){
    $redir->increment('clicks');
    return redirect($redir->dest, $redir->code);
  }else{
    return response('Not found', 404);
  }
});
