<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Redir extends Model
{
    use HasFactory;

    protected $attributes = [
      'slug' => '',
      'dest' => '',
      'code' => 302,
      'clicks' => 0,
    ];
}
